[[_TOC_]]


# introduction

This page is about performing a wholesale reset of a GitLab self-managed database schema.

## not recommended

2024-05-03

[-Review the issues in this project before using-]

Not recommended for use as currently written.

## why?

Customers and users of GitLab frequently run into issues during GitLab upgrades because their schema does not match the schema defined in the GitLab code base.

The intent of this process is to provide a way to create an accurate test environment to detect these issues prior to upgrading production.  [It's not intended for use in production](#scope).

## how?

PostgreSQL `pgdump` offers some features which, combined, can be used to re-write the schema of the database.

1. The data is exported in a format which is agnostic to some of the properties of the schema. For example, you can change the order of the columns, change the data types (to some extent), add in columns, or remove columns that aren't being used or referred to.
1. The schema can be exported in two phases.
   - The first phase is the table definitions
   - The second phase is the indexes and constraints

Together, these three exports are the component parts of a PostgreSQL database backup.

This process works by combining the data from one database with the schema from another.

## todo

Issue are likely to arise that testing has not yet identified.

Known unknowns include:

- Failure to apply constraints: if the database providing the data is missing constraints, or through other mechanisms has data in it which violate constraints, the application of the second part of schema will encounter issues.

# scope

This is not intended for production use.

This change separates the database schema from the data itself, and replaces the schema.

Where the definition of columns in tables changes as a result, the import process will cast the data 'on the fly'.

For example:

- From `timestamp without timezone` to `timestamp with timezone`.
- From `timestamp with timezone` to `timestamp without timezone`.

In these examples, it's necessary to create information not present in the data (in the first case, ie: the timezome) or discard information that was previously stored (the timezone, in the second case).

In this example, customers may find that date stamps in the UI (for historical data) then do not match up - such as the data/time on a git commit, and events in GitLab such as pipelines which resulted from that commit.

Or, data in GitLab won't match external sources such as Jenkins, Jira, or other integrated systems.

Future data will match, but historical data might become unreliable in this sort of way. As a result it's not recommended for production use.

It's intended for bringing testing environments in sync with production, so tests of GitLab upgrades are accurate.

# definitions

- Schema database: the database providing the schema. For example: your production environment.
- Target database: the database to be transformed, that's providing the data. For example, your test environment.
- Working schema database: temporary database used to adjust the schema as needed.
- Working import database: temporary database used to test.

# high level process

1. [Back up the target database](#create-recovery-point) 'as is' as a recovery point.
1. [Run a database backup from the target database](#dump-the-data), extracting just the data.
1. [Export the schema](#export-the-schema) from the schema database.
1. [Import the schema](#import-the-schema-for-modification) to a temporary working schema database.
1. [Ensure the partitions required for the source database](#create-missing-partitions) are created in the working schema database
1. [Export the 'pre' and 'post' schema definitions](#export-the-revised-schema) from the working schema database
1. [Import the schema and data](#test-importing-the-schema-and-data) into the working import database
1. [Investigate an errors](#investigate-errors) and fix the schema in the working schema database.
1. [Repeat the schema export and the test import](#repeat-the-schema-export-and-the-test-import).
1. [Repeat as needed](#repeat-as-needed) until the full import is successful.
1. [Perform the final import](#perform-the-import-into-the-target-database) into the target database.

End state: the target database's data is transformed and recast into the schema supplied by the schema database.

# detailed process

## create recovery point

Back up the target database (the one providing the data) 'as is' as a recovery point.

```
sudo gitlab-backup create SKIP=uploads,builds,artifacts,lfs,pages,repositories,registry
```

## dump the data

Run a database dump from the target database (the one providing the data) extracting just the data.

```
sudo -iu gitlab-psql bash
. /opt/gitlab/etc/gitlab-psql-rc

/opt/gitlab/embedded/bin/pg_dump \
   -p ${psql_port} -h ${psql_host} -d gitlabhq_production \
   --section=data > exported-data-$(date '+%Y%m%d%H%M')rev.sql
```

## export the schema

Export the schema from the schema database.

```shell
sudo -iu gitlab-psql bash
. /opt/gitlab/etc/gitlab-psql-rc

/opt/gitlab/embedded/bin/pg_dump \
   -p ${psql_port} -h ${psql_host} \
   -d gitlabhq_production -s > desired-schema-orig.sql
```

- copy `desired-schema.sql` to the test machine


## import the schema for modification

Import the schema to a temporary working schema database.

- become the database Linux account

```shell
sudo -iu gitlab-psql bash
. /opt/gitlab/etc/gitlab-psql-rc
```

- determine the database account to create databases as

```
/opt/gitlab/embedded/bin/psql -p ${psql_port} -h ${psql_host} -d gitlabhq_production
--- verify current owner
\l+ gitlabhq_production
```

- the owner will be passed as `-O gitlab` in subsequent commands.  Modify to the correct owner if yours is different.

- create working schema database

```shell
/opt/gitlab/embedded/bin/createdb -p ${psql_port} -h ${psql_host} -O gitlab wip-schema-db
```

- import the schema (exported from a GitLab instance)

```shell
/opt/gitlab/embedded/bin/psql -p ${psql_port} -h ${psql_host} \
   -d wip-schema-db -f desired-schema-orig.sql
```

## create missing partitions

- Ensure the partitions required for the source database are created in the working schema database
- This issue arises where the database supplying the schema doesn't have data from the same timeframe.  More than likely, the instance supplying the data has existed for longer than the instance supplying the schema.  It'll be less of a problem if it's the other way around.

- log into the database console and switch databases

```shell
sudo gitlab-psql
\l
-- for example
\connect wip-schema-db
\connect gitlabhq_production
```

- check what partitions exist in the source database and in the working schema database

```sql
select schemaname,tablename,tableowner from pg_catalog.pg_tables
  where schemaname not in ('pg_catalog','information_schema','public')
  order by schemaname,tablename;
```

### plan the changes needed

- IF it's a case of creating some additional partitions for later months, then it'll be SQL like this:

```sql
create table gitlab_partitions_dynamic.audit_events_202202
partition of audit_events for values from ('2022-02-01') to ('2022-03-01');
create table gitlab_partitions_dynamic.web_hook_logs_202202
partition of web_hook_logs for values from ('2022-02-01') to ('2022-03-01');
```

- If the schema's partitions start at a earier date than data you'll be inserting, the `000000` tables need dropping, the missing tables creating, and then the correct `000000` tables being created.

- For example, a fresh install in September 2021 has the following as the oldest partitions.  

```plaintext
(before)
        schemaname         |                tablename                 | tableowner 
---------------------------+------------------------------------------+------------
 gitlab_partitions_dynamic | audit_events_000000                      | gitlab
 gitlab_partitions_dynamic | audit_events_202109                      | gitlab
 gitlab_partitions_dynamic | audit_events_202110                      | gitlab
 gitlab_partitions_dynamic | web_hook_logs_000000                     | gitlab
 gitlab_partitions_dynamic | web_hook_logs_202109                     | gitlab
 gitlab_partitions_dynamic | web_hook_logs_202110                     | gitlab
```

- If the target database contains a couple of older partitions, then currently the `000000` tables own these date ranges. They need dropping, the missing paritions creating, and new `000000` tables creating.

```sql
drop table gitlab_partitions_dynamic.audit_events_000000;
drop table gitlab_partitions_dynamic.web_hook_logs_000000;
--
create table gitlab_partitions_dynamic.audit_events_202107
partition of audit_events for values from ('2021-07-01') to ('2021-08-01');
create table gitlab_partitions_dynamic.audit_events_202108
partition of audit_events for values from ('2021-08-01') to ('2021-09-01');
--
create table gitlab_partitions_dynamic.web_hook_logs_202107
partition of web_hook_logs for values from ('2021-07-01') to ('2021-08-01');
create table gitlab_partitions_dynamic.web_hook_logs_202108
partition of web_hook_logs for values from ('2021-08-01') to ('2021-09-01');
--
create table gitlab_partitions_dynamic.web_hook_logs_000000
partition of web_hook_logs for values from (MINVALUE) to ('2021-07-01');
create table gitlab_partitions_dynamic.audit_events_000000
partition of audit_events for values from (MINVALUE) to ('2021-07-01');
```

- It's probably easiest to write this SQL in a text file and then run it in on the command line.  Here's some example files with a range of partitions:

  - [audit_events.sql](assets/audit_events.sql) contains these SQL commands to create partitions for 2016-2021 inclusive, plus two months of 2022.
  - [web_hook_logs.sql](assets/web_hook_logs.sql) contains these SQL commands to create partitions for 2021-04 to 2022-02.

### modify the working schema database

Note: to ensure the partitions are owned by the right account, the unix account `git` is used, not `gitlab-psql`. This is because on Omnibus, `/var/opt/gitlab/postgresql/data/pg_ident.conf` specifies that the system username `git` maps to `gitlab`.  Trying to connect from `gitlab-psql` will be denied, with PostgreSQL logging:

```
LOG:  no match in usermap "gitlab" for user "gitlab" authenticated as "gitlab-psql"
```

- switch to the `git` account

```shell
sudo -iu git bash
. /opt/gitlab/etc/gitlab-psql-rc
```

- start interactive console session, or

```shell
/opt/gitlab/embedded/bin/psql -p ${psql_port} -h ${psql_host} --username=gitlab \
   -d wip-schema-db 
```

- apply the changes from a file

```shell
/opt/gitlab/embedded/bin/psql -p ${psql_port} -h ${psql_host} --username=gitlab \
   -d wip-schema-db -f partitionchanges.sql
```

### verify

```shell
sudo -iu gitlab-psql bash
. /opt/gitlab/etc/gitlab-psql-rc

/opt/gitlab/embedded/bin/psql -p ${psql_port} -h ${psql_host} \
   -d wip-schema-db 
```

```sql
-- check the data ranges are contiguous
\d+ web_hook_logs
\d+ audit_events
-- check ownership and the total count returned
select schemaname,tablename,tableowner from pg_catalog.pg_tables
  where schemaname not in ('pg_catalog','information_schema','public')
  order by schemaname,tablename;
-- compare with the count off the source database
\connect gitlabhq_production
select schemaname,tablename,tableowner from pg_catalog.pg_tables
  where schemaname not in ('pg_catalog','information_schema','public')
  order by schemaname,tablename;
```

## export the revised schema

- Export the 'pre' and 'post' schema definitions from the working schema database

```shell
sudo -iu gitlab-psql bash
. /opt/gitlab/etc/gitlab-psql-rc

/opt/gitlab/embedded/bin/pg_dump \
   -p ${psql_port} -h ${psql_host} \
   -d wip-schema-db --section=pre-data -s > desired-schema-pre-$(date '+%Y%m%d%H%M')rev.sql

/opt/gitlab/embedded/bin/pg_dump \
   -p ${psql_port} -h ${psql_host} \
   -d wip-schema-db --section=post-data -s > desired-schema-post-$(date '+%Y%m%d%H%M')rev.sql
```

## test importing the schema and data

`Import the schema and data into the working import database`

- create working import database

```shell
sudo -iu gitlab-psql bash
. /opt/gitlab/etc/gitlab-psql-rc
/opt/gitlab/embedded/bin/createdb -p ${psql_port} -h ${psql_host} -O gitlab  wip-import-db
```

- import the data  (the filenames can be tab-completed if you only have one set)

```shell
/opt/gitlab/embedded/bin/psql -p ${psql_port} -h ${psql_host} \
   -d wip-import-db \
   -f desired-schema-pre-YYYYMMDDHHMMrev.sql | \
 tee -a import-pre-$(date '+%Y%m%d%H%M').out

/opt/gitlab/embedded/bin/psql -p ${psql_port} -h ${psql_host} \
   -d wip-import-db \
   -f exported-data-YYYYMMDDHHMMrev.sql | \
 tee -a import-data-$(date '+%Y%m%d%H%M').out

/opt/gitlab/embedded/bin/psql -p ${psql_port} -h ${psql_host} \
   -d wip-import-db \
   -f desired-schema-post-YYYYMMDDHHMMrev.sql | \
 tee -a import-post-$(date '+%Y%m%d%H%M').out
```

### filter the output

Check for any errors.

The following greps out expected output, it should leave any errors behind.  The greppage does not filter out the following from the `pre` and `post` logs, the `data` log should return nothing.

```
 set_config 
------------
 
(1 row)
```

- PRE output

```shell
cat import-pre-YYYYMMDDHHMM.out | \
egrep -v '^ALTER TABLE$|^ALTER SEQUENCE$|^CREATE SEQUENCE$|^CREATE TABLE$|^COMMENT$|^CREATE VIEW$|^SET$|^CREATE FUNCTION$|^ALTER FUNCTION$|^CREATE EXTENSION$|^ALTER SCHEMA$|^CREATE SCHEMA$'
```

- Data output

```shell
cat import-data-YYYYMMDDHHMM.out | \
egrep -v '^\(1 row\)$|^-*$|^ setval *$|^COPY [0-9]*$|^ *[0-9]*$|^SET$|^ set_config $'
```

- POST output

```shell
cat import-post-YYYYMMDDHHMM.out | \
egrep -v '^ALTER TABLE$|^CREATE INDEX$|^ALTER INDEX$|^CREATE TRIGGER$|^SET$|^COMMENT$'
```

## investigate errors

### data import phase: ERROR: relation "schema.table_column_seq" does not exist

Examples:

```plaintext
ERROR:  relation "public.user_statuses_user_id_seq" does not exist
LINE 1: SELECT pg_catalog.setval('public.user_statuses_user_id_seq',...
ERROR:  relation "public.project_incident_management_settings_project_id_seq" does not exist
LINE 1: SELECT pg_catalog.setval('public.project_incident_management...
```

The primary keys on tables in GitLab are set to automatically increment in the database as sequences.

This error means that the data import tried to set the sequence properties so the next record import would be set to the correct value, but the sequence isn't defined by the schema.

Fix: add the missing sequence(s) to the working schema database

1. [Dump PRE schema from target database](#dump-pre-schema-from-target-database)
1. Taking the examples above, search for `user_statuses_user_id_seq` and `project_incident_management_settings_project_id_seq`
1. Expect `CREATE SEQUENCE`, `ALTER TABLE`, `ALTER SEQUENCE`, `ALTER TABLE ONLY .. ALTER COLUMN`
1. Apply these to the working schema database.

```sql
CREATE SEQUENCE public.user_statuses_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER TABLE public.user_statuses_user_id_seq OWNER TO gitlab;
ALTER SEQUENCE public.user_statuses_user_id_seq OWNED BY public.user_statuses.user_id;
ALTER TABLE ONLY public.user_statuses ALTER COLUMN user_id SET DEFAULT nextval('public.user_statuses_user_id_seq'::regclass);

CREATE SEQUENCE public.project_incident_management_settings_project_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER TABLE public.project_incident_management_settings_project_id_seq OWNER TO gitlab;
ALTER SEQUENCE public.project_incident_management_settings_project_id_seq OWNED BY public.project_incident_management_settings.project_id;
ALTER TABLE ONLY public.project_incident_management_settings ALTER COLUMN project_id SET DEFAULT nextval('public.project_incident_management_settings_project_id_seq'::regclass);
```

## repeat the schema export and the test import

If there were errors or issues to fix.

- Drop the working import database

```shell
sudo -iu gitlab-psql bash
. /opt/gitlab/etc/gitlab-psql-rc

/opt/gitlab/embedded/bin/dropdb -p ${psql_port} -h ${psql_host} wip-import-db
```

- [Export the 'pre' and 'post' schema definitions](#export-the-revised-schema) from the working schema database

- [Import the schema and data](#test-importing-the-schema-and-data) into the working import database)

## repeat as needed

If there were errors or issues to fix.

- [investigate errors](#investigate-errors)
- [repeat the schema export and the test import](#repeat-the-schema-export-and-the-test-import)

## perform the import into the target database

Having been through one or more test cycles and fixed the schema, the final step is to replace the real database that supplied the data with

- the tested schema definition
- the original data

The procedure is:

- stop GitLab

```shell
gitlab-ctl stop
gitlab-ctl start postgresql
```

- running as `gitlab-psql`:

```shell
sudo -iu gitlab-psql bash
. /opt/gitlab/etc/gitlab-psql-rc

- start a database console, connectigg to a different database (to avoid `ERROR:  current database cannot be renamed`) such as `-d postgres`, `-d wip-import-db`, or `-d wip-schema-db`

/opt/gitlab/embedded/bin/psql -p ${psql_port} -h ${psql_host} \
   -d postgres 
```

- rename the existing GitLab database

```sql
ALTER DATABASE gitlabhq_production RENAME TO gitlab_old_schema
\q
```
- create replacement (modelled [on Omnibus](https://gitlab.com/gitlab-org/omnibus-gitlab/-/blob/14.5.0+ee.0/files/gitlab-cookbooks/postgresql/resources/database.rb#L10))
- edit the owner (`-O gitlab`) if the existing database has a different owner

```shell
/opt/gitlab/embedded/bin/createdb -p ${psql_port} -h ${psql_host} -O gitlab gitlabhq_production
```

```shell
/opt/gitlab/embedded/bin/psql -p ${psql_port} -h ${psql_host} \
   -d gitlabhq_production  \
   -f desired-schema-pre-YYYYMMDDHHMMrev.sql | \
 tee -a import-pre-$(date '+%Y%m%d%H%M').out

/opt/gitlab/embedded/bin/psql -p ${psql_port} -h ${psql_host} \
   -d gitlabhq_production  \
   -f exported-data-YYYYMMDDHHMMrev.sql | \
 tee -a import-data-$(date '+%Y%m%d%H%M').out

/opt/gitlab/embedded/bin/psql -p ${psql_port} -h ${psql_host} \
   -d gitlabhq_production  \
   -f desired-schema-post-YYYYMMDDHHMMrev.sql | \
 tee -a import-post-$(date '+%Y%m%d%H%M').out
```

- [check the output](#filter-the-output) for errors

- test

1. Start a rails console, and quit.  IF there's an basic database issues, it'll fail to initialize.

```shell
sudo gitlab-rails c
```

2. Run some checks

Any checks run after a [GitLab restore](https://docs.gitlab.com/ee/raketasks/backup_restore.html#restore-for-omnibus-gitlab-installations) are relevant here.

```shell
gitlab-rake gitlab:doctor:secrets
gitlab-ctl restart
# wait for GitLab to start
sudo gitlab-rake gitlab:check SANITIZE=true
```

## delete databases once no longer required

```
sudo -iu gitlab-psql bash
. /opt/gitlab/etc/gitlab-psql-rc
/opt/gitlab/embedded/bin/dropdb -p ${psql_port} -h ${psql_host} wip-schema-db
/opt/gitlab/embedded/bin/dropdb -p ${psql_port} -h ${psql_host} wip-import-db
/opt/gitlab/embedded/bin/dropdb -p ${psql_port} -h ${psql_host} gitlab_old_schema
```

# additional information and procedures

This information is referred to as required.

## useful commands

This process involves creating some databases, so it can be useful to switch between them

- `\l`, `\l+` list all databases, the latter with extra info like size
- `\connect NAME` switches databases


## dump PRE schema from target database

This isn't a required step, it's needed as part of certain fixes.

```shell
sudo -u gitlab-psql  /opt/gitlab/embedded/bin/pg_dump -h /var/opt/gitlab/postgresql \
-d gitlabhq_production \
--section=pre-data > target_schema_pre.sql
```

## using the reference schema from the GitLab code repo

TL;DR: it didn't work.  If you want to reset to a clean schema, build a self-managed instance from scratch (via a package, or docker) to the required version, and export the schema.

- The GitLab rails code repo contains a `sql` definition of the database schema, 'the reference schema'. For example: [13.12.0-ee](https://gitlab.com/gitlab-org/gitlab/-/blob/v13.12.0-ee/db/structure.sql).
- This procedure was tested using that as the schema, rather than a dump from GitLab, but some issues were found

the process worked as follows

- [The schema selected for import is the reference schema](#import-the-schema-for-modification) for the GitLab version you're running, instead of your dumping your existing schema
- [Work was needed to ensure the partitions required for the source database](#create-missing-partitions) were created.
  - Only `gitlab_partitions_static` schema partitions are included
  - [audit_events.sql](assets/audit_events.sql) contains these SQL commands to create partitions for 2016-2021 inclusive, plus two months of 2022.
  - [web_hook_logs.sql](assets/web_hook_logs.sql) contains these SQL commands to create partitions for 2021-04 to 2022-02.
  - all the required dynamic partitions have to be created, including the `000000` tables, so it's similar to the normal process, except without needing to drop them first.
- Running through the process worked fine until you try to fire up GitLab: everything's owned by the `gitlab-psql` account (on an Omnibus install) - GitLab doesn't access the database with this account.

```
active_record/connection_adapters/postgresql_adapter.rb:675:in `exec_params': PG::InsufficientPrivilege: ERROR:  permission denied for table licenses (ActiveRecord::StatementInvalid)
```

- A dump from GitLab sets the permissions on the tables and other objects within the SQL. The reference schema does not.
- I tried changing the process to use the application account instead, so the objects were all created as if GitLab had created them.
- Issues were encountered there .. it looks like it's a requirement to create the objects using the superuser, and then set the ownership to the application account.

```
ERROR:  data type bigint has no default operator class for access method "gist"
HINT:  You must specify an operator class for the index or define a default operator class for the data type.
```
